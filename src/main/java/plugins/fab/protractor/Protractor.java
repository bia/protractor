/**
 * by Fabrice de Chaumont
 */

package plugins.fab.protractor;

import icy.gui.dialog.MessageDialog;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginImageAnalysis;
import icy.sequence.Sequence;

public class Protractor extends Plugin implements PluginImageAnalysis {

	@Override
	public void compute() {


		Sequence sequence = getFocusedSequence();
		
		if ( sequence!=null )
		{
			new ProtractorPainter( getFocusedSequence() );
		}else
		{
			MessageDialog.showDialog("Please open an image first.", MessageDialog.INFORMATION_MESSAGE );
		}
		
	}

}
